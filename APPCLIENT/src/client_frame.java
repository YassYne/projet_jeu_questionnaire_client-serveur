import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class client_frame extends javax.swing.JFrame 
{
    private static List<Question> tableau_Qst;
    int [ ] mesReponses = new int[10];
	String username, address = "localhost";
    ArrayList<String> users = new ArrayList();
    int port = 2222;
    Boolean isConnected = false;
    
    Socket sock;
    BufferedReader reader;
    PrintWriter writer;
	private ButtonGroup buttonGroup;
	private static int question = 0;
	private static int score = 0;
    
    //--------------------------//
    
    public void ListenThread() 
    {
         Thread Client = new Thread(new Client());
         Client.start();
    }
    
    //--------------------------//
    
    public void userAdd(String data) 
    {
         users.add(data);
    }
    
    //--------------------------//
    
    public void userRemove(String data) 
    {
         ta_chat.append(data + " Hors connexion.\n");
    }
    
    //--------------------------//
    
    public void writeUsers() 
    {
         String[] tempList = new String[(users.size())];
         users.toArray(tempList);
         for (String token:tempList) 
         {
             //users.append(token + "\n");
         }
    }
    
    //--------------------------//
    
    public void sendDisconnect() 
    {
        String bye = (username + ": :se d�connecter");
        try
        {
            writer.println(bye); 
            writer.flush(); 
        } catch (Exception e) 
        {
            ta_chat.append("ne peut pas envoyer le msg de d�connexion.\n");
        }
    }

    //--------------------------//
    
    public void Disconnect() 
    {
        try 
        {
            ta_chat.append("D�connexion.\n");
            sock.close();
        } catch(Exception ex) {
            ta_chat.append("D�connexion �chou�. \n");
        }
        isConnected = false;
        tf_username.setEditable(true);

    }
    
    public client_frame() 
    {
        initComponents();
    }
    
    //--------------------------//
    private int score1 = -1;
    private int score2 = -1;
    public class Client implements Runnable
    {
        

		@Override
        public void run() 
        {
            String[] data;
            String stream, done = "Done", connect = "Connect", disconnect = "Disconnect", chat = "Chat", resultat = "Resultat";

            try 
            {
                while ((stream = reader.readLine()) != null) 
                {
                 	

                    data = stream.split(":");
                    if(data[2].equals(resultat)){
                    	System.out.println("Je recois le resultat: " +data[1]);
                    	if(score1 == -1)
                    		score1 = Integer.valueOf(data[1]);
                    	else
                    		score2 = Integer.valueOf(data[1]);
                    	
                    	fin();
                    }else if (data[2].equals(chat)) 
                     {
                        ta_chat.append(data[0] + ": " + data[1] + "\n");
                        ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
                        
                     } 
                     else if (data[2].equals(connect))
                     {
                        ta_chat.removeAll();
                        userAdd(data[0]);
                     } 
                     else if (data[2].equals(disconnect)) 
                     {
                         userRemove(data[0]);
                     } 
                     else if (data[2].equals(done)) 
                     {
                        //users.setText("");
                        writeUsers();
                        users.clear();
                     } 
                    	
                  
                }
           }catch(Exception ex) { }
        }
    }

    //--------------------------//
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb_address = new javax.swing.JLabel();
        tf_address = new javax.swing.JTextField();
        lb_port = new javax.swing.JLabel();
        tf_port = new javax.swing.JTextField();
        lb_username = new javax.swing.JLabel();
        tf_username = new javax.swing.JTextField();
        lb_password = new javax.swing.JLabel();
        tf_password = new javax.swing.JTextField();
        b_connect = new javax.swing.JButton();
        b_disconnect = new javax.swing.JButton();
        b_anonymous = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        ta_chat = new javax.swing.JTextArea();
        tf_chat = new javax.swing.JTextField();
        b_send = new javax.swing.JButton();
        b_reponse = new javax.swing.JButton();
        lb_name = new javax.swing.JLabel();
        buttonGroup = new ButtonGroup();
        
		btnRep1 = new JRadioButton("R�ponse 1");
		btnRep2 = new JRadioButton("R�ponse 2");
		btnRep3 = new JRadioButton("R�ponse 3");
		btnRep4 = new JRadioButton("Sans R�ponse");
		
		btnRep1.setActionCommand("1");
		btnRep2.setActionCommand("2");
		btnRep3.setActionCommand("3");
		btnRep4.setActionCommand("4");
		btnRep4.setSelected(true);
		btnRep4.setVisible(false);
		
		buttonGroup.add(btnRep1);
		buttonGroup.add(btnRep2);
		buttonGroup.add(btnRep3);
		buttonGroup.add(btnRep4);
		
		panBtnsRadio.add(btnRep1);
		panBtnsRadio.add(btnRep2);
		panBtnsRadio.add(btnRep3);
		panBtnsRadio.add(btnRep4);
		
		panBtnsRadio.setVisible(false);
		b_reponse.setVisible(false);
		
		b_reponse.setText("R�pondre");
		//b_reponse.setVisible(false);
		
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Jeu Questionnaire");
        setName("client"); // NOI18N
        setResizable(false);
        lb_address.setText("adresse : ");

        tf_address.setText("localhost");
        tf_address.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_addressActionPerformed(evt);
            }
        });

        lb_port.setText("Port :");

        tf_port.setText("2222");
        tf_port.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_portActionPerformed(evt);
            }
        });

        lb_username.setText("Nom :");

        tf_username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_usernameActionPerformed(evt);
            }
        });

        lb_password.setText("Mot de passe : ");

        b_connect.setText("Se connecter");
        b_connect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_connectActionPerformed(evt);
            }
        });

        b_disconnect.setText("D�connexion");
        b_disconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_disconnectActionPerformed(evt);
            }
        });

        b_anonymous.setText("Anonymous authentification");
        b_anonymous.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_anonymousActionPerformed(evt);
            }
        });

        ta_chat.setColumns(20);
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);

        b_send.setText("Envoyer ton message");
        b_send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_sendActionPerformed(evt);
            }
        });
        
        b_reponse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_reponseActionPerformed(evt);
            }
        });

        lb_name.setText("Bienvenu");
        lb_name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tf_chat, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(b_send, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                    		)
                    .addGroup(layout.createSequentialGroup()
                            .addComponent(panBtnsRadio, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_reponse, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                        		)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lb_username, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                            .addComponent(lb_address, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_address, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                            .addComponent(tf_username))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lb_password, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lb_port, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_password)
                            .addComponent(tf_port, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(b_connect)
                                .addGap(2, 2, 2)
                                .addComponent(b_disconnect)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(b_anonymous, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lb_name)
                .addGap(201, 201, 201))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb_address)
                    .addComponent(tf_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_port)
                    .addComponent(tf_port, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(b_anonymous))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tf_username)
                    .addComponent(tf_password)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lb_username)
                        .addComponent(lb_password)
                        .addComponent(b_connect)
                        .addComponent(b_disconnect)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_chat)
                    .addComponent(b_send, javax.swing.GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE)
                		)
                
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panBtnsRadio)
                        .addComponent(b_reponse, javax.swing.GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE)
                    		)
                
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lb_name))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    

	private void tf_addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_addressActionPerformed
       
    }//GEN-LAST:event_tf_addressActionPerformed

    private void tf_portActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_portActionPerformed
   
    }//GEN-LAST:event_tf_portActionPerformed

    private void tf_usernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_usernameActionPerformed
    
    }//GEN-LAST:event_tf_usernameActionPerformed
    
    private void b_connectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_connectActionPerformed
        if (isConnected == false) 
        {
        	
            username = tf_username.getText();
            tf_username.setEditable(false);

            try 
            {
                sock = new Socket(address, port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println(username + ":est connect�.:Connect");
                writer.flush(); 
                isConnected = true; 
                
        		InputStream is = sock.getInputStream();
        		ObjectInputStream ois = new ObjectInputStream(is);
        		Object objetRecu = ois.readObject();
        		List<Question> Table = (List<Question>) objetRecu;
        		tableau_Qst = Table;
        		String str = "Question N � 1 : "+ tableau_Qst.get(0).getquest()+
        				"\n 1- "+tableau_Qst.get(0).getRepons1()+
        				"\n 2- "+tableau_Qst.get(0).getRepons2()+
        				"\n 3- "+tableau_Qst.get(0).getRepons3()+
        				"\n ----------------------------------- \n Chat avec ton amis :";
        		ta_chat.setText(str);

        		panBtnsRadio.setVisible(true);
        		b_reponse.setVisible(true);
        		
            }
            catch (Exception ex) 
            {
                ta_chat.append("Impossible de se connecter! R�essayer. \n");
                tf_username.setEditable(true);
            }
            
            ListenThread();
            
        } else if (isConnected == true) 
        {
            ta_chat.append("Vous etes deja connect� . \n");
        }
    }//GEN-LAST:event_b_connectActionPerformed

    protected void b_reponseActionPerformed(ActionEvent evt) {
    	
    	String reponse = buttonGroup.getSelection().getActionCommand();    		
    	
		
		if(reponse != null)
		verifier(question,reponse);
		else{
			mesReponses[question] = 4 ;
			++question;
		}
		
		
    	if(question<10){
    	int numQST =question+1;
    	String str = "Question N � "+numQST+" : "+ tableau_Qst.get(question).getquest()+
				"\n 1- "+tableau_Qst.get(question).getRepons1()+
				"\n 2- "+tableau_Qst.get(question).getRepons2()+
				"\n 3- "+tableau_Qst.get(question).getRepons3()+
				"\n ----------------------------------- \n Chat avec ton amis :";
		ta_chat.setText(str);
		btnRep4.setSelected(true);
    	}else{
    		String tmp = ""+score;
    		System.out.println("j'ai fini j'e vais envoyer le message : "+tmp);
    		envoyerMsg(tmp,"Resultat");
    		System.out.println("le message a ete envoye: "+tmp);
    		
    		fin();
    	}
    	
	}
    
    private void fin() {
    	String str = "attendre resultat";
    	if(question>=10){
		ta_chat.setText("");
		b_reponse.setVisible(false);
		panBtnsRadio.setVisible(false);
		
		if(score1 != -1 && score2 != -1){
			ta_chat.setText("");
				if(score1 == score2){
					str = "Egalite, votre score est : "+score+".\n Le score de ton adversaire est : "+score2+"\n";
				}else{
					if(score == score1){
						
						if(score>score2){
						str = "Bravo vous avez gagn�, votre score est : "+score+" pts.\n Le score de ton adversaire est : "+score2+"\n";	
						}else{
							str = "D�sol� vous avez perdu, votre score est : "+score+" pts.\n Le score de ton adversaire est : "+score2+"\n";
						}
					}else{
						if(score>score1){
							str = "Bravo vous avez gagn�, votre score est : "+score+" pts.\n Le score de ton adversaire est : "+score1+"\n";
							}else{
								str = "D�sol� vous avez perdu, votre score est : "+score+" pts.\n Le score de ton adversaire est : "+score1+"\n";
							}
					}
				}
			System.out.println(str);
		
		String srep= "";
		
		ta_chat.setText(str+"\n -------------------------- \n \n");

		for(int i =0; i<tableau_Qst.size() && i<mesReponses.length;i++){
			int n = i+1;
			
			if(mesReponses[i] == 4){
				srep = "Sans r�ponse";
			}else{
				srep = ""+mesReponses[i];
			}
			
			str = "Question N � "+n+" : "+ tableau_Qst.get(i).getquest()+
					"\n 1- "+tableau_Qst.get(i).getRepons1()+
					"\n 2- "+tableau_Qst.get(i).getRepons2()+
					"\n 3- "+tableau_Qst.get(i).getRepons3()+
					"\n La bonne R�ponse : "+ tableau_Qst.get(i).getBonnerep()+
					"\n Votre R�ponse : "+ srep+
					"\n \n";
			ta_chat.setText(ta_chat.getText()+str);
			
			b_reponse.setVisible(false);
		}
    	
    	
    	}else{
    		ta_chat.setText(str);
    	}
    	}
    	
	}

	private void verifier(int question2, String reponse) {
    	
    	int a = Integer.valueOf(reponse);
    	mesReponses[question2] = a ;
		if(question2<10)
		if(a == tableau_Qst.get(question2).getBonnerep()){
			++score;
		}
		
		b_reponse.setText("R�pondre, Votre score : "+score+" pts");
    	++question;

	}

	private void b_disconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_disconnectActionPerformed
        sendDisconnect();
        Disconnect();
    }//GEN-LAST:event_b_disconnectActionPerformed

    private void b_anonymousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_anonymousActionPerformed
        tf_username.setText("");
        if (isConnected == false) 
        {
            String anon="anon";
            Random generator = new Random(); 
            int i = generator.nextInt(999) + 1;
            String is=String.valueOf(i);
            anon=anon.concat(is);
            username=anon;
            
            tf_username.setText(anon);
            tf_username.setEditable(false);

            try 
            {
                sock = new Socket(address, port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println(anon + ":est connect�.:Connect");
                writer.flush(); 
                isConnected = true; 
                
                InputStream isk = sock.getInputStream();
        		ObjectInputStream ois = new ObjectInputStream(isk);
        		Object objetRecu = ois.readObject();
        		List<Question> Table = (List<Question>) objetRecu;
        		tableau_Qst = Table;
        		String str = "Question N � 1 : "+ tableau_Qst.get(0).getquest()+
        				"\n 1- "+tableau_Qst.get(0).getRepons1()+
        				"\n 2- "+tableau_Qst.get(0).getRepons2()+
        				"\n 3- "+tableau_Qst.get(0).getRepons3()+
        				"\n ----------------------------------- \n Chat avec ton amis :";
        		ta_chat.setText(str);
        		
        		panBtnsRadio.setVisible(true);
        		b_reponse.setVisible(true);
        		
            } 
            catch (Exception ex) 
            {
                ta_chat.append("Impossible de se connecter! R�essayer. \n");
                tf_username.setEditable(true);
            }
            
            ListenThread();
            
        } else if (isConnected == true) 
        {
            ta_chat.append("Vous �tes d�ja connect�. \n");
        }        
    }//GEN-LAST:event_b_anonymousActionPerformed

    private void b_sendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_sendActionPerformed
    	String msg = tf_chat.getText();
    	envoyerMsg(msg,"Chat");
    }//GEN-LAST:event_b_sendActionPerformed

    private void envoyerMsg(String msg, String type) {
    	String nothing = "";
        
    	if (!msg.equals(nothing)) {
    		try {
    			System.out.println("Transmission");
                writer.println(username + ":" + msg + ":" + type);
                writer.flush(); // flushes the buffer
                System.out.println("fin Transmission");
                } catch (Exception ex) {
                    ta_chat.append("Message n'a pas �t� envoy�. \n");
                }
    	}
    	
        tf_chat.setText("");
        tf_chat.requestFocus();
	}

	public static void main(String args[]) throws ClassNotFoundException 
    {	
    	
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() 
            {
                new client_frame().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_anonymous;
    private javax.swing.JButton b_connect;
    private javax.swing.JButton b_disconnect;
    private javax.swing.JButton b_send;
    private javax.swing.JButton b_reponse;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lb_address;
    private javax.swing.JLabel lb_name;
    private javax.swing.JLabel lb_password;
    private javax.swing.JLabel lb_port;
    private javax.swing.JLabel lb_username;
    private javax.swing.JTextArea ta_chat;
    private JPanel panBtnsRadio = new JPanel(new GridLayout(1,1));
    private javax.swing.JTextField tf_address;
    private javax.swing.JTextField tf_chat;
    private JRadioButton btnRep1,btnRep2, btnRep3, btnRep4;
    private javax.swing.JTextField tf_password;
    private javax.swing.JTextField tf_port;
    private javax.swing.JTextField tf_username;
    
    // End of variables declaration//GEN-END:variables
}
